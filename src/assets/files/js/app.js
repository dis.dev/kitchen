//Расчёт кухни
//отключаем действие клика по умолчанию на ссылке
//получаем цену по клику
$(document).on('click', '.getPrice', (function (e) {
    $(this).parents('.list-buttons').find('.getPrice').removeClass('active');
    $(this).addClass('active');
    e.stopPropagation();
    var action = 'getPrice_new';
    var rid = $(this).attr('data-rid');
    var name = $(this).attr('data-name');
    var block = $(this).parents('.product');

    $.ajax({
        type: "POST",
        url: '/ajax/',
        data: { action: action, rid: rid, name: name},
        dataType: "json",
        success: function (data) {
            //var data = JSON.parse(data);
            //console.log(data);
            if (data) {
                if (action === 'getPrice_new') {
                    $(block).find('.price-dev').html(data.price);
                    $(block).find('.price-of-metr').html(data.price_of_metr);
                }
            }
        }
    });
}));
$(document).on('click', '.calc_item svg, .calc_item svg path ' , function (e) {
    e.preventDefault();
    parent = $(e.target).closest('div.calc_item');
    console.log(parent);
    //первый шаг - выбор формы
    if(parent.hasClass('straight')){
        $('.side-b').show();
        $('#calculator_order input[name=side-a],#calculator_order input[name=side-c]').val(0);
        $('.side-a, .side-c').hide();
    } else if(parent.hasClass('corner')){
        $('#calculator_order input[name=side-a],#calculator_order input[name=side-b]').val(500);
        $('.side-b, .side-a').show();

        $('#calculator_order input[name=side-c]').val(0);
        $('.side-c').hide();

    } else if(parent.hasClass('n-shaped')){
        $('#calculator_order input[name=side-a],#calculator_order input[name=side-b],#calculator_order input[name=side-c]').val(500);
        $('.side-b, .side-a, .side-c').show();
    }
    Calc();
});
Calc();

$('.calculatorBody  input').keyup(function(){
    Calc();
});
$(document).on('change', '#calculator_order select, #calculator_order input' , function (e) {
    Calc();
});

function Calc() {
    var price = $('select[name=material]').val();
    var lengthKitchenA = Number($('input[name=side-a]').val());
    var lengthKitchenB = Number($('input[name=side-b]').val());
    var lengthKitchenC = Number($('input[name=side-c]').val());
    var topCloset = Number($('input[name=topCloset]:checked').val());

    var lengthKitchen = lengthKitchenA + lengthKitchenB + lengthKitchenC;
    var result = (price * lengthKitchen) * topCloset;
    $('.calc__sum h6 price').text(result);
    console.log($('input[name=topCloset]:checked').val());
    // console.log(lengthKitchen);
    // console.log(result);

    $('#modal_register input[name=material]').val($('select[name=material] option:selected').text());
    $('#modal_register input[name=side_a]').val(lengthKitchenA);
    $('#modal_register input[name=side_b]').val(lengthKitchenB);
    $('#modal_register input[name=side_c]').val(lengthKitchenC);
    $('#modal_register input[name=topCloset]').val(topCloset);
    $('#modal_register input[name=price]').val(result);

}

const toggleClass = (element, className, addClass) => element?.classList.toggle(className, addClass);
const hasClass = (element, className) => element?.classList.contains(className);
const defaultClasses = {
  active: 'active',
  hidden: 'hidden',
  remove: 'remove'
};
const getAllElements = (selector, parent) => parent ? parent.querySelectorAll(selector) : document.querySelectorAll(selector);
const getElementUtil = (selector, parent) => parent ? parent.querySelector(selector) : document.querySelector(selector);
const getParent = (item, selector) => item.closest(selector);
const getMatchesParent = (item, selector, selector2) => item.closest(selector) || item.matches(selector2 ? selector2 : selector);
const cloneElement = element => element.cloneNode(true);
const addLast = (list, element) => list[list.length - 1].after(element);
const setText = (element, text) => element.innerHTML = text;
const setId = (element, id) => element.id = id;
const setAttribute = (element, attribute, text) => element.setAttribute(attribute, text);
const replaceText = (element, needle, replaceValue) => element.replace(needle, replaceValue);
const calcInit = () => {
  const calc = document.querySelector('.calc');
  const calcWrapper = calc?.querySelector('.calc__wrapper');
  const calcItems = calcWrapper?.querySelectorAll('.calc_item');
  const calcBody = calc?.querySelector('.calc__form-wrapper');
  if (!calc || !calcWrapper || !calcItems || !calcBody) return;
  calcItems.forEach(item => {
    console.log(item);
    item.addEventListener('click', e => {
      const parent = e.target.matches('.calc_item') || e.target.closest('.calc_item');
      calcItems.forEach(calcItem => toggleClass(calcItem, defaultClasses.active, false));
      toggleClass(parent, defaultClasses.active, true);
      toggleClass(calcBody, defaultClasses.hidden, false);
    });
  });
};
calcInit();
const certrtificateInit = () => {
  const certificateBlock = document.querySelector('.certificate');
  const certificateCards = certificateBlock?.querySelectorAll('.certificate_cards');
  const certificateFg = certificateBlock?.querySelector('.certificate__view .fg');
  const certificateBg = certificateBlock?.querySelector('.certificate__view .bg');
  if (!certificateBlock || !certificateCards || !certificateFg || !certificateBg) return;
  certificateCards.forEach(card => {
    card.addEventListener('click', e => {
      setAttribute(certificateFg, 'src', e.target.getAttribute('src'));
      setAttribute(certificateBg, 'src', e.target.getAttribute('src'));
    });
  });
};
certrtificateInit();
const initFullscreenModal = () => {
  const attr = {
    dataModalSource: 'data-modal-source',
    dataModalSourceType: 'data-modal-source-type',
    dataModalTrigger: 'data-modal-trigger'
  };
  const fullscreenModal = document.querySelector('.fullscreen-modal');
  const fullscreenModalTriggers = document.querySelectorAll('.modal-trigger[data-modal-trigger="fullscreen"]');
  const modalContent = fullscreenModal?.querySelector('.modal__content');
  const modalImg = modalContent?.querySelector('.fullscreen-image');
  const modalVideo = modalContent?.querySelector('.fullscreen-video');
  const modalVideoSource = modalVideo?.querySelector('source');
  const modalNext = fullscreenModal?.querySelector('#next-image');
  const modalPrev = fullscreenModal?.querySelector('#prev-image');
  const fullscreen = fullscreenModal?.querySelector('#fullscreen');
  const modalIndicator = fullscreenModal?.querySelector('.fullscreen-indicator');
  if (!fullscreenModal || !modalContent || !fullscreenModalTriggers.length) return;
  let contentInfo = [];
  const setContent = itemInfo => {
    if (itemInfo.sourceType === 'img') {
      toggleClass(modalImg, 'hidden', false);
      toggleClass(modalVideo, 'hidden', true);
      setAttribute(modalImg, 'src', itemInfo.source);
    } else {
      toggleClass(modalImg, 'hidden', true);
      toggleClass(modalVideo, 'hidden', false);
      setAttribute(modalVideoSource, 'src', itemInfo.source);
      modalVideo.load();
    }
    setAttribute(fullscreenModal, 'data-id', itemInfo.id);
    setText(modalIndicator, `${itemInfo.id + 1}/${contentInfo.length}`);
  };
  const pushContent = () => {
    fullscreenModalTriggers.forEach(item => {
      item.addEventListener('click', e => {
        const target = hasClass(e.target, 'modal-trigger') ? e.target : e.target.closest('.modal-trigger');
        const id = Number(target.getAttribute('data-id'));
        const itemInfo = contentInfo.find(item => item.id === id);
        setContent(itemInfo);
      });
    });
  };
  const getContentInfo = () => {
    fullscreenModalTriggers.forEach((item, index) => {
      item.setAttribute('data-id', `${index}`);
      contentInfo.push({
        id: index,
        source: item.getAttribute(attr.dataModalSource),
        sourceType: item.getAttribute(attr.dataModalSourceType)
      });
    });
  };
  const nextSlide = () => {
    let currentId = Number(fullscreenModal.getAttribute('data-id'));
    if (currentId === contentInfo.length - 1) currentId = -1;
    setContent(contentInfo.find(item => item.id === currentId + 1));
  };
  const prevSlide = () => {
    let currentId = Number(fullscreenModal.getAttribute('data-id'));
    if (currentId === 0) currentId = contentInfo.length;
    setContent(contentInfo.find(item => item.id === currentId - 1));
  };
  const toggleFullScreen = () => {
    if (!document.fullscreenElement) document.documentElement.requestFullscreen();else if (document.exitFullscreen) document.exitFullscreen();
  };
  getContentInfo();
  pushContent();
  modalNext.addEventListener('click', nextSlide);
  modalPrev.addEventListener('click', prevSlide);
  fullscreen.addEventListener('click', e => toggleFullScreen());
};
initFullscreenModal();
const initKitchen = () => {
  const kitchen = document.querySelector('.kitchen__screen');
  const kitchenScrollable = kitchen?.querySelector('.scrollable');
  const kitchenNext = kitchen?.querySelector('#kitchen-arrow-next');
  const kitchenPrev = kitchen?.querySelector('#kitchen-arrow-prev');
  if (!kitchen) return;
  kitchenNext.addEventListener('click', () => kitchenScrollable.scrollTop += 170);
  kitchenPrev.addEventListener('click', () => kitchenScrollable.scrollTop -= 170);
  const service = document.querySelector('.service');
  const serviceScrollable = service?.querySelector('.service__list');
  const serviceNext = service?.querySelector('#service-arrow-next');
  const servicePrev = service?.querySelector('#service-arrow-prev');
  if (!service) return;
  serviceNext.addEventListener('click', () => serviceScrollable.scrollTop += 155);
  servicePrev.addEventListener('click', () => serviceScrollable.scrollTop -= 155);
};
initKitchen();
const questionInit = () => {
  const form = document.querySelector('.test__form');
  const formBody = form?.querySelectorAll('.test__form-body');
  const formButtonPrev = form?.querySelector('.test__button_prev');
  const formButtonNext = form?.querySelector('.test__button_next');
  // const formButtonReset = form?.querySelector('.test__button_reset');
  const formIndicator = form?.querySelector('.test__form-indicator');
  if (!form || !formBody || !formButtonPrev || !formButtonNext || !formIndicator) return;
  let currentSlide = 0;
  const reset = () => {
    formBody.forEach((body, index) => {
      const select = body.querySelectorAll('input');
      select.forEach(button => button.checked = false);
      if (index === 0) {
        toggleClass(body, defaultClasses.hidden, false);
        toggleClass(formIndicator, defaultClasses.hidden, false);
        toggleClass(formButtonPrev, defaultClasses.hidden, true);
        toggleClass(formButtonNext, defaultClasses.hidden, false);
        // toggleClass(formButtonReset, defaultClasses.hidden, true);
        setText(formIndicator, `${index + 1}/${formBody.length}`);
        currentSlide = 0;
      } else toggleClass(body, defaultClasses.hidden, true);
    });
  };
  reset();
  //form.addEventListener('submit', e => e.preventDefault());
  formButtonNext.addEventListener('click', () => {
    if (currentSlide + 1 >= formBody.length) return;
    currentSlide++;
    formBody.forEach((body, index) => {
      if (index === currentSlide) {
        toggleClass(body, defaultClasses.hidden, false);
        toggleClass(formButtonPrev, defaultClasses.hidden, false);
        setText(formIndicator, `${index + 1}/${formBody.length}`);
      } else toggleClass(body, defaultClasses.hidden, true);
    });
    if (currentSlide + 1 === formBody.length) {
      toggleClass(formIndicator, defaultClasses.hidden, true);
      toggleClass(formButtonPrev, defaultClasses.hidden, true);
      toggleClass(formButtonNext, defaultClasses.hidden, true);
      // toggleClass(formButtonReset, defaultClasses.hidden, false);
    }
  });
  formButtonPrev.addEventListener('click', () => {
    if (currentSlide - 1 < 0) return;
    currentSlide--;
    formBody.forEach((body, index) => {
      if (index === currentSlide) {
        toggleClass(body, defaultClasses.hidden, false);
        setText(formIndicator, `${index + 1}/${formBody.length}`);
      } else toggleClass(body, defaultClasses.hidden, true);
      if (index === 0) toggleClass(formButtonPrev, defaultClasses.hidden, true);
    });
  });
  // formButtonReset.addEventListener('click', () => reset());
};
questionInit();
const selects = document.querySelectorAll('.form-control');
selects.forEach(i => {
  document.addEventListener('DOMContentLoaded', function () {
    new Choices(i, {
      allowHTML: true,
      searchEnabled: false,
      itemSelectText: ''
    });
  });
});
const initSidebar = () => {
  const sidebarWrapper = document.querySelector('.sidebar__wrapper');
  const sidebars = sidebarWrapper?.querySelectorAll('.sidebar_2');
  const triggers = sidebarWrapper?.querySelectorAll('.sidebar-list-trigger');
  const sidebarOverlay = sidebarWrapper?.querySelector('.sidebar__overlay');
  const sidebarOpen = document.querySelector('.sidebar__open');
  const sidebar = sidebarWrapper?.querySelector('.sidebar');
  const closeTriggers = sidebarWrapper?.querySelectorAll('.sidebar2__close');
  if (!sidebarWrapper) return;
  const open = trigger => {
    sidebars.forEach(item => toggleClass(item, 'hidden', true));
    triggers.forEach(item => toggleClass(item, 'active', false));
    sidebars.forEach(item => {
      if (item.getAttribute('data-id') !== trigger.getAttribute('data-id')) return;
      toggleClass(item, 'hidden', false);
      toggleClass(trigger, 'active', true);
      toggleClass(sidebarWrapper, 'active', true);
    });
  };
  const close = () => {
    sidebars.forEach(item => toggleClass(item, 'hidden', true));
    triggers.forEach(item => toggleClass(item, 'active', false));
    toggleClass(sidebarWrapper, 'active', false);
  };
  const openSide = () => sidebar.addEventListener('click', () => toggleClass(sidebarWrapper, 'active', true));
  const hoverSidebar = () => {
    sidebars.forEach(item => toggleClass(item, 'hidden', true));
    triggers.forEach(item => toggleClass(item, 'active', false));
    sidebarOverlay.addEventListener('click', () => close());
    toggleClass(sidebarWrapper, 'active', false);
    let isOpen = false;
    triggers.forEach(trigger => {
      const arrow = trigger.querySelector('.sidebar__arrow');
      arrow.addEventListener('click', e => {
        if (!hasClass(sidebarWrapper, 'active') && !hasClass(trigger, 'active') || hasClass(sidebarWrapper, 'active') && !hasClass(trigger, 'active')) {
          open(trigger);
          isOpen = true;
        } else if (hasClass(sidebarWrapper, 'active') && hasClass(trigger, 'active')) {
          close();
          isOpen = false;
        }
      });
    });
  };
  const tapSidebar = () => {
    sidebars.forEach(item => toggleClass(item, 'hidden', true));
    triggers.forEach(item => toggleClass(item, 'active', false));
    if (!sidebarOpen) return;
    sidebar.addEventListener('click', () => toggleClass(sidebarWrapper, 'active', true));
    sidebarOpen.addEventListener('click', () => toggleClass(sidebarWrapper, 'active', true));
    sidebarOverlay.addEventListener('click', () => toggleClass(sidebarWrapper, 'active', false));
    triggers.forEach(trigger => {
      const arrow = trigger.querySelector('.sidebar__arrow');
      arrow.addEventListener('click', () => open(trigger));
    });
    closeTriggers.forEach(trigger => {
      trigger.addEventListener('click', () => {
        sidebars.forEach(item => toggleClass(item, 'hidden', true));
        triggers.forEach(item => toggleClass(item, 'active', false));
      });
    });
  };
  const hoverSidebarHandler = () => window.innerWidth >= 640 ? hoverSidebar() : tapSidebar();
  const hoverSidebarHandler2 = () => window.innerWidth >= 640 && window.innerWidth <= 768 ? openSide() : false;
  hoverSidebarHandler();
  window.removeEventListener('resize', hoverSidebarHandler);
  window.addEventListener('resize', hoverSidebarHandler);
  window.removeEventListener('resize', hoverSidebarHandler2);
  window.addEventListener('resize', hoverSidebarHandler2);
};
initSidebar();
const toggleModal = () => {
  const triggers = getAllElements('.modal-trigger');
  const modals = getAllElements('.modal__wrapper');
  const activeClass = 'modal__active';
  const toggleModalHandler = triggerName => {
    modals.forEach(modal => {
      const modalName = modal.getAttribute('data-modal');
      if (triggerName === modalName) toggleClass(modal, activeClass, true);else toggleClass(modal, activeClass, false);
    });
  };
  triggers.forEach(trigger => {
    const triggerName = trigger.getAttribute('data-modal-trigger');
    trigger.removeEventListener('click', () => toggleModalHandler(triggerName));
    trigger.addEventListener('click', () => toggleModalHandler(triggerName));
  });
};
const closeModalHandler = e => {
  const parentModalClass = '.modal__wrapper';
  const closeModalClass = '.close-modal-trigger';
  const activeClass = 'modal__active';
  if (e.target.matches(`.${activeClass}`) || e.target.matches(closeModalClass)) {
    const parent = e.target.closest(parentModalClass);
    toggleClass(parent, activeClass, false);
  }
};
const closeModal = modal => toggleClass(modal, 'modal__active', false);
const showModal = modal => toggleClass(modal, 'modal__active', true);
document.removeEventListener('click', closeModalHandler);
document.addEventListener('click', closeModalHandler);
toggleModal();