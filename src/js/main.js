"use strict"

const calcInputs = document.querySelectorAll('[data-calc-input]')
if (calcInputs.length > 0 ) {
    calcInputs.forEach(elem => {
        elem.addEventListener('change', (event) => {
            const calc = event.target.closest('[data-calc]')

            const calcBodyField = calc.querySelector('[data-calc-body-price]')
            const calcFrontField = calc.querySelector('[data-calc-front-price]')
            let  bodySquare = calc.querySelector('[data-calc-body-amount]').value
            let  frontSquare = calc.querySelector('[data-calc-front-amount]').value

            calcBodyField.innerHTML = (bodySquare * parseInt(calcBodyField.dataset.calcBodyPrice))
            calcFrontField.innerHTML = (frontSquare * parseInt(calcFrontField.dataset.calcFrontPrice))

            calc.querySelector('[data-calc-total]').innerHTML = (bodySquare * parseInt(calcBodyField.dataset.calcBodyPrice) + frontSquare * parseInt(calcFrontField.dataset.calcFrontPrice))
        })
    })
}

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-poll-nav]')) {
        const poll = event.target.closest('[data-poll-section]')
        const step = event.target.closest('[data-poll-nav]').dataset.pollNav
        const steps = poll.querySelectorAll('[data-poll]')

        steps.forEach(elem => {
            elem.classList.remove('-active-');
        });
        poll.querySelector(`[data-poll="${step}"]`).classList.add('-active-');
    }
})

function quantity() {

    document.addEventListener('click',  (event) => {
        if(event.target.closest('[data-quantity-button]')) {
            const quantity = event.target.closest('[data-quantity]')
            const quantityInput = quantity.querySelector('[data-quantity-input]')
            const quantityMin = quantityInput.min
            const quantityMax = quantityInput.max
            const quantityDirection = event.target.closest('[data-quantity-button]').dataset.quantityButton
            let quantityValue = 0;


            if(quantityDirection === 'minus') {
                quantityValue = parseInt(quantityInput.value) - 1
                if (quantityValue < quantityMin) {
                    quantityValue = quantityMin
                }
            }
            else {
                quantityValue = parseInt(quantityInput.value) + 1
                if (quantityValue > quantityMax) {
                    quantityValue = quantityMax
                }
            }
            quantityInput.value = quantityValue
        }
    })

    if( document.querySelector('[data-quantity-input]')) {

        document.querySelector('[data-quantity-input]').addEventListener('change', function (){
            let val = parseInt(this.value)
            const quantity = this.closest('[data-quantity]').querySelector('[data-quantity-input]')

            if(!val) {
                val = quantity.min
            }

            if (val < quantity.min) {
                val = quantity.min
            }
            else if (val > quantity.max) {
                val = quantity.max
            }

            quantity.value = val
        })
    }
};

function initRange() {

    const dataSliders = document.querySelectorAll('.data-slider');
    if (dataSliders.length > 0) {
        dataSliders.forEach(el => {
            let sliderStart = el.getAttribute('data-start').split(",").map(parseFloat);
            let sliderMin = parseFloat(el.getAttribute('data-min'));
            let sliderMax = parseFloat(el.getAttribute('data-max'));
            noUiSlider.create(el, {
                start: sliderStart,
                connect: 'lower',
                range: {
                    'min': sliderMin,
                    'max': sliderMax,
                }
            });
        });
    }

    const rangeSliders = document.querySelectorAll('[data-range]');
    if (rangeSliders.length > 0) {
        rangeSliders.forEach(el => {
            const range = el
            const rangeInput = range.querySelector('[data-range-input]')
            const rangeSlider = range.querySelector('[data-range-slider]')
            const rangeStart = parseFloat(rangeSlider.getAttribute('data-start'));
            const rangeMin   = parseFloat(rangeSlider.getAttribute('data-min'));
            const rangeMax   = parseFloat(rangeSlider.getAttribute('data-max'));

            noUiSlider.create(rangeSlider, {
                start: rangeStart,
                connect: 'lower',
                step: 1,
                range: {
                    'min': rangeMin,
                    'max': rangeMax,
                }
            });

            rangeSlider.noUiSlider.on('update', function (values, handle) {
                rangeInput.value = parseInt(values[handle])
            });

            rangeInput.addEventListener('change', function () {
                rangeSlider.noUiSlider.set([this.value]);
            });
        });
    }
}

function initSliders() {

    if (document.querySelector('[data-media-slider]')) {
        let mediaQuerySize  = 720;
        let mediaSlider = null;

        function mediaSliderInit() {
            if(!mediaSlider) {
                mediaSlider = new Swiper('[data-media-slider]', {
                    init: true,
                    observer: true,
                    observeParents: true,
                    slidesPerView: 1,
                    spaceBetween: 20,
                    autoHeight: true,
                    speed: 400,
                    pagination: {
                        el: "[data-media-pagination]",
                        clickable: true,
                    },
                });
            }
        }

        function mediaSliderDestroy() {
            if(mediaSlider) {
                mediaSlider.destroy();
                mediaSlider = null;
            }
        }

        if (document.documentElement.clientWidth < mediaQuerySize) {
            mediaSliderInit()
        }

        window.addEventListener('resize', function (){

            if (document.documentElement.clientWidth < mediaQuerySize) {
                mediaSliderInit()
            }
            else {
                mediaSliderDestroy()
            }
        });
    }

}


window.addEventListener("load", function (e) {
    initRange()
    initSliders()
    quantity()

    // Modal Fancybox
    Fancybox.bind('[data-fancybox]', {
        autoFocus: false
    });

    document.querySelectorAll('[data-date]').forEach(el => {
        new AirDatepicker(el,{
            autoClose: true,
            inline: false
        });
    });
});

