# Gulp стартовый шаблон nunjucks, scss


## Требования
* [Node.js](https://nodejs.org/en/)
* [npm](https://www.npmjs.com/)
* [gulp](https://gulpjs.com/)
* [git](https://git-scm.com/)

## Установка
1. Склонировать репозиторий
2. Установить зависимости — npm i
3. Запустить сервер разработки — npm run dev

## Команды
* ```npm run dev``` - запуск сервера разработки
* ```npm run build``` - полная сборка без запуска сервера разработки
